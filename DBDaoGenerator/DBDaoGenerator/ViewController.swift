//
//  ViewController.swift
//  DBDaoGenerator
//
//  Created by Renton on 24/04/2018.
//  Copyright © 2018 Renton. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    var config = TemplateConfig()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //前面是数据库的列名，后面是model的key名
//        let modelMapping: [String: String] = ["": "",
//                                                "": ""]
        do {
            let data = try Data(contentsOf: Bundle.main.url(forResource: "templete", withExtension: "input")!)
            let template = String(data: data, encoding: .utf8)
            assert(template != nil)
            let ret = replaceTemplete(templete: template!, keys: config.templateKeys)
            print(ret)
            let url = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("\(config.daoName).output")
            print(url.absoluteString)
            try ret.write(to: url, atomically: true, encoding: .utf8)
            
        } catch {
            print(error)
            print("error loading templete")
        }
    }
    
    func replaceTemplete(templete:String, keys:[String]) -> String {
        var ret = templete
        for key in keys {
            ret = ret.replacingOccurrences(of: key, with: config.valueForTempleteKey(key: key))
        }
        return ret
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

