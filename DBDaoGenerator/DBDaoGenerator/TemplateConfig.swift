//
//  TemplateConfig.swift
//  DBDaoGenerator
//
//  Created by Renton on 2018/6/2.
//  Copyright © 2018 Renton. All rights reserved.
//

import Foundation

class TemplateConfig {
    //下面是template的keys
    let kDaoName = "#DaoName"
    let kTableName = "#TableName"
    let kColumnDefine = "#ColumnDefine"
    let kTableInit = "#TableInit"
    let kModelNameWithFirstUpcase = "#ModelNameWithFirstUpcase"
    let kModelNameLowerCase = "#ModelNameLowerCase"
    let kModelName = "#ModelName"
    let kInsertOrUpdateValueSql = "#InsertOrUpdateValueSql"
    let kInsertOrUpdateSqlValueMapping = "#InsertOrUpdateSqlValueMapping"
    let kRemoveSql = "#RemoveSql"
    let kRemoveQueryKeys = "#RemoveQueryKeys"
    let kSearchQueryKeyTypeArr = "#SearchQueryKeyTypeArr"
    let kSearchQuerySql = "#SearchQuerySql"
    let kParseFromDB = "#ParseFromDB"
    let kRemoveModelKeyName = "#RemoveModelKeyName"
    let kSearchModelKeyName = "#SearchModelKeyName"
    let kCheckModelValid = "#CheckModelValid"
    
    //##########
    //You define your own properties from here
    //########
    
    //下面是template里面对应的values及需要的一些东西
    let daoName = "LikedCourseDao"
    let dbName = "t_liked_course"
    //前面是列名，后面是Sql里面的type
    //type只支持Text,Number,INTEGER
    //example [("id", "Text"),
//    ("timestamp", "Int"),
//    ("package", "String"),
//    ("session", "String"),
//    ("sessionIndex", "Int"),
//    ("sessionId", "String")]
    let dbStructure: [(String, String)] = [("id", "Text"),
                                           ("json", "String")]
    let primayKey = "id"
    let modelName = "Package"
    let modelVarName = "package"
    
    //save
    //model properties
    let notNilProperties: [String] = ["id"]
    
    //model的key和对应的type,在从db读出来的时候使用对应的type来取值,如果是对象，填Json
    //现在支持 string int ClassName
    //example
//    let modelStructure: [(String, String)] = [("id", "string"),
//                                              ("timestamp", "int"),
//                                              ("package", "Package"),
//                                              ("session", "Session"),
//                                              ("sessionIndex", "int"),
//                                              ("sessionId", "string"),
//                                              ]
    let modelStructure: [(String, String)] = [("id", "string"),
                                              ("haha", "String")]
    
    let removeModelKeyName = "id"
    let removeModelKeyType = "String"
    //db
    let removeColumnKey = "id"
    
    let searchModelKeyName = "id"
    let searchModelKeyType = "String"
    //db
    let searchColumnKey = "id"
    
    //##############
    //All completed~~~~ Just run~~~~
    //##############
    
    var templateKeys: [String] {
        get {
            return [kDaoName,
                     kTableName,
                     kColumnDefine,
                     kTableInit,
                     kModelNameWithFirstUpcase,
                     kModelNameLowerCase,
                     kModelName,
                     kInsertOrUpdateValueSql,
                     kInsertOrUpdateSqlValueMapping,
                     kRemoveSql,
                     kRemoveQueryKeys,
                     kSearchQuerySql,
                     kParseFromDB,
                     kRemoveModelKeyName,
                     kSearchModelKeyName,
                     kSearchQueryKeyTypeArr,
                     kCheckModelValid
                ]
        }
    }
    
    func valueForTempleteKey(key:String) -> String {
        var ret = ""
        switch key {
        case kDaoName:
            ret = daoName
        case kTableName:
            ret = dbName
        case kColumnDefine:
            var arr:[String] = []
            for k in dbStructure {
                arr.append("private let collumn_\(k.0) = \"\(k.0)\"")
            }
            ret = arr.joined(separator:"\n")
        case kTableInit:
            var arr:[String] = []
            for k in dbStructure {
                arr.append("\(k.0) \(k.1)")
            }
            arr.append("PRIMARY KEY (\(primayKey))")
            ret = arr.joined(separator:", ")
        case kModelNameLowerCase:
            ret = modelVarName
        case kModelName:
            ret = modelName
        case kInsertOrUpdateValueSql:
            var collumns:[String] = []
            for k in dbStructure {
                collumns.append(k.0)
            }
            ret = "(" + collumns.joined(separator: ", ") + ")"
            var t = [String]()
            for _ in 0..<collumns.count {
                t.append("?")
            }
            ret.append(" values (\(t.joined(separator: ", ")))")
        case kInsertOrUpdateSqlValueMapping:
            let keys = modelStructure.map { (key, _) -> String in
                return "\(modelVarName).\(key)"
            }
            ret = keys.joined(separator: ", ")
        case kRemoveSql:
            ret = removeColumnKey
        case kRemoveQueryKeys:
            ret = "\(removeModelKeyName):\(removeModelKeyType)"
        case kSearchQueryKeyTypeArr:
            ret = "\(searchModelKeyName):\(searchModelKeyType)"
        case kSearchQuerySql:
            ret = searchColumnKey
        case kParseFromDB:
            ret.append("guard ")
            var t = [String]()
            for i in 0..<dbStructure.count {
                let collumnKey = dbStructure[i].0
                let modelKey = modelStructure[i].0
                let modelType = modelStructure[i].1
                switch modelType {
                case "int":
                    t.append("let \(modelKey) = Int(resultSet.int(forColumn: \"\(collumnKey)\"))")
                case "string":
                    t.append("let \(modelKey) = resultSet.string(forColumn: \"\(collumnKey)\")")
                default:
                    t.append("let \(modelKey)Json = resultSet.json(forColumn: \"\(collumnKey)\")")
                }
                
            }
            ret.append(t.joined(separator: ",\n"))
            ret.append(" else {\ncontinue\n}\n")
            ret.append("let element = \(modelName)()\n")
            for i in 0..<modelStructure.count {
                let modelType = modelStructure[i].1
                if modelType == "int" || modelType == "string" {
                    ret.append("element.\(modelStructure[i].0) = \(modelStructure[i].0)\n")
                } else {
                    ret.append("let \(modelStructure[i].0) = \(modelStructure[i].1)()\n")
                    ret.append("\(modelStructure[i].0).fillFromJson(jsonData: \(modelStructure[i].0)Json)\n")
                    ret.append("element.\(modelStructure[i].0) = \(modelStructure[i].0)\n")
                }
            }
        case kRemoveModelKeyName:
            ret = removeModelKeyName
        case kSearchModelKeyName:
            ret = searchModelKeyName
        case kCheckModelValid:
            if notNilProperties.count > 0 {
                let v = notNilProperties.map { (key) in
                    return "let _ = \(modelVarName).\(key)"
                }.joined(separator: ",\n")
                ret = "guard \(v) else { return false }"
            } else {
                ret = "\n"
            }
        default:print("matching for \(key) not found!!!\n\n")
        }
        return ret
    }
}
